﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerClicking : MonoBehaviour {

    public int numberOfClothes;
    public GameObject content;
    public List<GameObject> elementsUI;
	
	public void ClickUiElement(int index)
    {
        for (int i = 0; i < elementsUI.Count; i++)
        {
            elementsUI[i].gameObject.GetComponent<Image>().enabled = false;
        }

        elementsUI[index].gameObject.GetComponent<Image>().enabled = true;
        loadItems(getItemsOfResources(numberOfClothes));
    }

    private void loadItems(List<GameObject> items)
    {
        int childs = content.transform.childCount;
        for (int i = 0; i < childs; i++)
        {
            GameObject.Destroy(content.transform.GetChild(i).gameObject);
        }

        for (int i = 0; i < items.Count; i++)
        {
            GameObject item = Instantiate(items[i]);
            item.transform.SetParent(content.transform, false);
        }
    }

    private List<GameObject> getItemsOfResources(int numberOfClothes)
    {
        switch (numberOfClothes)
        {
            case 0:
                Debug.Log("Load list of helmets from shop");
                return listOfAllItems("Equipment/Shop/Helmet");
            case 1:
                Debug.Log("Load list of shields from shop");
                return listOfAllItems("Equipment/Shop/Shield");
            case 2:
                Debug.Log("Load list of pants from shop");
                return listOfAllItems("Equipment/Shop/Pants");
            case 3:
                Debug.Log("Load list of amulets from shop");
                return listOfAllItems("Equipment/Shop/Amulet");
            case 4:
                Debug.Log("Load list of weapons from shop");
                return listOfAllItems("Equipment/Shop/Weapon");
            case 5:
                Debug.Log("Load list of shoes from shop");
                return listOfAllItems("Equipment/Shop/Shoes");
            default:
                Debug.Log("Popraw dane");
                return null;
        }
        return new List<GameObject>();
    }

    private List<GameObject> listOfAllItems(string path)
    {
        List<GameObject> items = new List<GameObject>();
        int iterator = 0;
        bool empty = false;

        do
        {
            GameObject item = Resources.Load<GameObject>(path + "_" + iterator);
            if (item == null)
                empty = true;
            else
            {
                iterator++;
                items.Add(item);
            }
        } while (!empty);

        return items;
    }
}
