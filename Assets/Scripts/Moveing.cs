﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moveing : MonoBehaviour {

    private Rigidbody2D myRig2d;

    [SerializeField]
    private float movementSpeed;

	void Start () {
        myRig2d = GetComponent<Rigidbody2D>();
        transform.position = new Vector3(0.0f, 0.0f, 0.0f);
    }
	
	void Update () {
        float horizontal = Input.GetAxis("Horizontal");
        float jump = Input.GetAxis("Jump");

        Jump(jump);
        HandleMovement(horizontal);
	}

    private void HandleMovement(float horizontal)
    {
        myRig2d.velocity = new Vector2(horizontal *movementSpeed, myRig2d.velocity.y);
    }

    private void Jump(float jump, float force = 4.0f)
    {
        if (CollisionGround.isGround)
        {
            myRig2d.AddForce(new Vector2(0, jump * force), ForceMode2D.Impulse);
            CollisionGround.isGround = false;
        }
    }
}
