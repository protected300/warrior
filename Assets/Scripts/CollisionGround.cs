﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionGround : MonoBehaviour {
    public static bool isGround;

    void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground") isGround = true;
    }
}
