﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadImageToEquipment : MonoBehaviour {
    //Percent size of slot sprite
    private const int percent = 80;
    float result;

    public void WearingHero(string itemName)
    {
        GameObject hero = GameObject.Find("Hero");
        GameObject slot = GameObject.Find(getSlotName(itemName.ToCharArray()));

        string path = "Equipment/Clothes/" + itemName;
        GameObject image = Instantiate(Resources.Load<GameObject>(path));
        Sprite image1 = Instantiate(Resources.Load<Sprite>("Sprites/" + itemName));

        int childs = hero.transform.childCount;
        for (int i = 0; i < childs; i++)
        {
            if (hero.transform.GetChild(i).tag == gameObject.tag)
                GameObject.Destroy(hero.transform.GetChild(i).gameObject);
        }

        image.transform.SetParent(hero.transform, false);
        slot.GetComponent<Image>().sprite = image1;
        slot.GetComponent<Image>().SetNativeSize();

        float width = slot.GetComponent<Image>().sprite.bounds.size.x;
        float height = slot.GetComponent<Image>().sprite.bounds.size.y;
        if (width < height)
        {
            result = (1 / height) *percent;
            slot.GetComponent<RectTransform>().sizeDelta = new Vector2(width *= result, height *= result);
        }
        else
        {
            result = (1 / width) *percent;
            slot.GetComponent<RectTransform>().sizeDelta = new Vector2(width *= result, height *= result);
        }
        rewearing(hero);
    }

    private string getSlotName(char[] itemName)
    {
        string name = "";
        for (int i = 7; i < itemName.Length; i++)
        {
            if (itemName[i].Equals('_'))
                return "Img" + name;

            name += itemName[i];
        }
        return null;
    }

    private void rewearing(GameObject hero)
    {
        List<GameObject> clothes = new ArrayList<GameObject>();

        foreach (Transform child in hero.transform)
        {
            clothes.Add(child.gameObject);
        }

        for (int i = 0; i <= 4; i++)
        {
            foreach (GameObject obj in clothes)
            {
                if (obj.GetComponent<LayerShowingClothes>().layer == i)
                {
                    obj.transform.SetAsLastSibling();
                }
            }
        }
    }
}
